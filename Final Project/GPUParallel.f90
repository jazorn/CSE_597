!Jacob Zorn
!Poisson's Equation Solver

include "GPUParallelMod.f90"

program main

	use GPUParallelMod
	use openacc
	use HDF5
	
	implicit none
	include 'mpif.h'
	
	integer											:: i,j,k,iter,maxIter, nx, ny, CBC, CSOL, IFlag
	double precision, dimension(:,:), allocatable	:: bMatrix, xMatrix, xMatrixOld
	integer											:: columns_glo, columns_loc, rows
	
	character(len=:), allocatable					:: filename
	
	integer											:: rank, nprocs, ierr,column_start, column_end
	integer											:: status(MPI_Status_Size)
	integer											:: left, right, file_len
	integer											:: gpu_rank, gpu_type, gpu_size
	
	double precision								:: eps, dx, dy
	double precision								:: gloRes, locRes, Res
	double precision								:: start_time, stop_time
	
	!MPI setup and start up
	call MPI_Init(ierr)
	call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)
	
	!MPI communciation tags
	left = 101
	right = 100
	
	!OpenACC setup and start up
	gpu_size = 1
	gpu_type = acc_get_device_type()
	gpu_size = acc_get_num_devices(gpu_type)
	
	call acc_init(acc_device_nvidia)
	gpu_size = acc_get_num_devices(acc_device_nvidia)
	gpu_rank = mod(rank,gpu_size)
	call acc_set_device_num(gpu_rank,acc_device_nvidia)
	
	if (rank.eq.0) then
		print*, 'The number of processors running is: ', nprocs
		print*, 'The number of gpus I have running is: ', gpu_size
		start_time = MPI_Wtime()
		call Read_Input_File(nx, ny, CBC, CSOL, dx, dy, IFlag, maxIter)
		eps = 0.0001
	endif
	
	call Setup_Broadcast(nx, ny, CBC, CSOL, dx, dy, IFlag, eps, maxIter)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	
	call Setup_General(columns_glo,rows,columns_loc,column_start,column_end,nx,ny,nprocs,rank)
	
	call Setup_Matrix(xMatrix,xMatrixOld,bMatrix,columns_glo,rows)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	
	do i = 0,nprocs-1
		if (rank.eq.i) then
			print*, '-----------------------------------------------------'
			print*, 'My gpu_rank is: ', gpu_rank,' on mpi_rank: ', rank,'.'
			print*, 'column_end: ', column_end,' and column_start: ', column_start
			print*, '-----------------------------------------------------'
		endif
	enddo

	locRes = 0.0
	gloRes = 1.0
	iter = 1
	
	!$acc data copy(xMatrix,xMatrixOld,bMatrix)
	do while (gloRes.ge.eps .and. iter.le.maxIter)
	
		call FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy,rows,nx,ny)
		
		!!$acc update host(xMatrix(:,column_start),xMatrix(:,column_end))
		
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		if (nprocs.eq.1) then
			!Don't do anything
		else
			!Start communication phase
			
			!First send far right column to immediately forward processor
			if (rank.lt.nprocs-1) then
				!$acc update host(xMatrix(:,column_end))
				call MPI_Send(xMatrix(1,column_end),rows,MPI_Double_Precision,rank+1,right,MPI_COMM_WORLD,ierr)
			endif
			if (rank.gt.0) then
				call MPI_Recv(xMatrixOld(1,column_start-1),rows,MPI_Double_Precision,rank-1,right,MPI_COMM_WORLD,status,ierr)
				!$acc update device(xMatrixOld(:,column_start-1))
			endif
			
			!Now send far left column to immediately previous processor
			if (rank.gt.0) then
				!$acc update host(xMatrix(:,column_start))
				call MPI_Send(xMatrix(1,column_start),rows,MPI_Double_Precision,rank-1,left,MPI_COMM_WORLD,ierr)
			endif
			if (rank.lt.nprocs-1) then
				call MPI_Recv(xMatrixOld(1,column_end+1),rows,MPI_Double_Precision,rank+1,left,MPI_COMM_WORLD,status,ierr)
				!$acc update device(xMatrixOld(:,column_end+1))
			endif
		endif
		
		!!$acc update device(xMatrixOld(:,)
        call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		call DiffFind(xMatrix,column_end,column_start,xMatrixOld,locRes,rows,rank, nx, ny)

		call MPI_Reduce(locRes,gloRes,1,MPI_Double_Precision,MPI_MAX,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(gloRes,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
				
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		iter = iter + 1
	enddo
	!$acc end data
	
	
	if (rank.eq.0) then
		stop_time = MPI_Wtime()
		print*, 'The number of iterations necessary was ', iter
		print*, 'The max error was ', gloRes
		print*, 'Total time to compute was ', stop_time - start_time,' seconds.'
	endif
	
	if (rank.eq.0) then
		start_time = MPI_Wtime()
	endif
	
	print*, size(xMatrix,1), size(xMatrix,2)
	file_len = len('hdf5work.h5')
	allocate(character(len=file_len) :: filename)
	
	filename = 'hdf5work.h5'
	
	call HDF5_Write_Array(trim(filename),file_len, rank, nprocs, column_start, column_end, xMatrix)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	call MPI_Finalize(ierr)
	
end program main