#CSE597 Final Report
#Written by Jacob Zorn (jzz164@psu)
#Written: 12/4/2018
#Make File for Serial Parallel Poisson Solver

#In order for this to work, you must have access to the module listed below (GPU_HDF5). As it is built with PGI-OpenMPI-HDF5. This compiler was built specifically for this project.

FC=h5pfc

SOURCES=GPUParallel.f90
OBJECTS=$(SOURCES:.f=.o)
EXECUTABLE=GPUSolver.exe

all:
	module use /home/jzorn/work/Modules/ModuleFiles/; \
	module load GPU_HDF5; \
	$(FC) -acc -O4 -fastsse GPUParallel.f90 -o GPUSolver.exe 

clean:
	rm *o *.exe
