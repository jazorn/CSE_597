!Jacob Zorn
!Poisson's Equation Solver

include "ParallelMod.f90"

program main

	use ParallelMod
	implicit none
	include 'mpif.h'
	
	integer											:: i,j,k,iter,maxIter, nx, ny, CBC, CSOL, IFlag
	double precision, dimension(:,:), allocatable	:: bMatrix, xMatrix, xMatrixOld
	integer											:: columns_glo, columns_loc, rows
	
	integer											:: rank, nprocs, ierr,column_start, column_end
	integer											:: status(MPI_Status_Size)
	integer											:: left, right
	
	double precision								:: eps, dx, dy
	double precision								:: gloRes, locRes, Res
	double precision								:: start_time, stop_time
	
	call MPI_Init(ierr)
	call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierr)
	call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)
	
	left = 101
	right = 100
	
	if (rank.eq.0) then
		print*, 'The number of processors running is: ', nprocs
		start_time = MPI_Wtime()
		call Read_Input_File(nx, ny, CBC, CSOL, dx, dy, IFlag, maxIter)
		eps = 0.0001
	endif
	
	call Setup_Broadcast(nx, ny, CBC, CSOL, dx, dy, IFlag, eps, maxIter)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	
	call Setup_General(columns_glo,rows,columns_loc,column_start,column_end,nx,ny,nprocs,rank)
	
	call Setup_Matrix(xMatrix,xMatrixOld,bMatrix,columns_glo,rows)
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)

	locRes = 0.0
	gloRes = 1.0
	iter = 1
	
	do while (gloRes.ge.eps .and. iter.le.maxIter)
	
		call FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy, rows,nx,ny)

		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		if (nprocs.eq.1) then
			!Don't do anything
		else
			!Start communication phase
			
			!First send far right column to immediately forward processor
			if (rank.lt.nprocs-1) then
				call MPI_Send(xMatrix(1,column_end),rows,MPI_Double_Precision,rank+1,right,MPI_COMM_WORLD,ierr)
			endif
			if (rank.gt.0) then
				call MPI_Recv(xMatrixOld(1,column_start-1),rows,MPI_Double_Precision,rank-1,right,MPI_COMM_WORLD,status,ierr)
			endif
			
			!Now send far left column to immediately previous processor
			if (rank.gt.0) then
				call MPI_Send(xMatrix(1,column_start),rows,MPI_Double_Precision,rank-1,left,MPI_COMM_WORLD,ierr)
			endif
			if (rank.lt.nprocs-1) then
				call MPI_Recv(xMatrixOld(1,column_end+1),rows,MPI_Double_Precision,rank+1,left,MPI_COMM_WORLD,status,ierr)
			endif
		endif
		
        call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		
		if (mod(iter,5).eq.1) then
			call DiffFind(xMatrix,column_end,column_start,xMatrixOld,locRes,rows,rank, nx, ny)
			call MPI_Reduce(locRes,gloRes,1,MPI_Double_Precision,MPI_MAX,0,MPI_COMM_WORLD,ierr)
			call MPI_Bcast(gloRes,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		else
			xMatrixOld(:,column_start:column_end) = xMatrix(:,column_start:column_end)
		endif
				
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
		iter = iter + 1
	enddo
	
	if (rank.eq.0) then
		stop_time = MPI_Wtime()
		print*, 'The number of iterations necessary was ', iter
		print*, 'The max error was ', gloRes
		print*, 'Total time to compute was ', stop_time - start_time,' seconds.'
	endif
	
	if (rank.ne.0) then
		call MPI_Send(xMatrix(1,column_start),rows*columns_loc,MPI_Double_Precision,0,200+rank,MPI_COMM_WORLD,ierr)
	endif
	
	if (rank.eq.0) then
		do k = 1,nprocs-1
			call MPI_Recv(xMatrix(1,(k)*columns_loc+1),rows*columns_loc,MPI_Double_Precision,k,200+k,MPI_COMM_WORLD,status,ierr)
		enddo
	endif
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	
	! do k = 1,nprocs-1
		! call MPI_Send(xMatrix(1,j
		! if (rank.eq.k) then
			! do j = column_start, column_end
				! print*, j
				! call MPI_Send(xMatrix(1,j),rows,MPI_Double_Precision,0,right,MPI_COMM_WORLD,ierr)
				! call MPI_Recv(xMatrix(1,j),rows,MPI_Double_Precision,k,right,MPI_COMM_WORLD,status,ierr)
			! enddo
		! endif
		! call MPI_Barrier(MPI_COMM_WORLD,ierr)
	! enddo
	
	! if (rank.eq.0) then
		! print*, xMatrix(10,98)
	! endif
	
	if (rank.eq.0) then
		open(unit=13,file='xMatrix.dat',status='replace',action='write')
			do i = 1,rows
				do j = 1,columns_glo
					write(13,"(10F10.4)",advance='no') xMatrix(i,j)
				enddo
				write(13,*) ''
			enddo
		close(unit=13)
	endif
	
	call MPI_Barrier(MPI_COMM_WORLD,ierr)
	call MPI_Finalize(ierr)
	
end program main