# CSE_597 Progress Report 2
#Basic Information

Author: Jacob Zorn
Date: Nov. 4, 2018

The licensing information is in the attached license.txt file and is licensed under the GNU General Public License.

This program is a Poisson's Equation solver with Drichlet Boundary conditions in with parallelization accomplished using MPI.

#Compilation instructions for ACI

Note: For maximum performance, it is recommended to compile using PGI compilers with various optimization flags. However, the accompanying MakeFile uses the GNU family of Fortran compilers. The best flag was found to be either O2 or O3.

Make sure to read through the accompanying documentation and update the input.in file prior to running the program.

make
mpirun -n #NumberofProcessors./Solver.exe

#Expected Output
Produces a xMatrix.dat file. Running the accompanying Matlab file (Plotter.m) will produce a PNG image of the results. The Matlab file has been packaged inside a shell script to make running it easier if necessary. Just make sure to make the shell script executable using the appropriate CHMOD command.

#To Compile the LaTex Progress Report
./pdfmake.sh
