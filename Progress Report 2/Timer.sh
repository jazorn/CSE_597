#! /bin/bash

SizeVector=( 100 200 300 400 500 600 700 800 900 1000 )
ProcVector=( 1 2 3 4 5 6 7 8 9 10 )

module load gcc/5.3.1
module load mpich

I=0
for SIZER in "${SizeVector[@]}"
do
	echo size:$SIZER
	cp ../input.in .
	sed -i "s/nny/${SIZER}/g" input.in
	sed -i "s/[""]//g" input.in
	procs="${ProcVector[$I]}"

	mpirun -n $procs ./Solver.exe
	I=$I+1
done
