module ParallelMod
	implicit none
	
	contains
	
	subroutine DiffFind_FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy,rows,nx,ny,locRes)
		implicit none
		
		integer											:: i, j, nx, ny
		integer											:: column_end, column_start, columns_glo, rows, rank, nprocs
		double precision								:: locRes, dx, dy
		double precision, dimension(nx,ny)				:: bMatrix, xMatrix, xMatrixOld
	
		if (rank.eq.0 .and. nprocs.eq.1) then
			do j = 2,columns_glo-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		elseif (rank.eq.0 .and. nprocs.ne.1) then
			do j = column_start+1,column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		elseif (rank.eq.nprocs-1) then
			do j = column_start,column_end-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		else
			do j = column_start, column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
					locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
					xMatrixOld(i,j) = xMatrix(i,j)
				enddo
			enddo
		endif
	
	end subroutine DiffFind_FDM
	subroutine DiffFind(xMatrix,column_end,column_start,xMatrixOld,locRes,rows,rank, nx, ny)
		
		implicit none
		
		integer											:: i,j, nx, ny
		integer											:: column_end, column_start, rows, rank
		double precision								:: locRes
		double precision, dimension(nx,ny)				:: xMatrix, xMatrixOld
		locRes = 0.0
		do j = column_start,column_end
			do i = 1,rows
				locRes = max(abs(xMatrix(i,j)-xMatrixOld(i,j)),locRes)
				xMatrixOld(i,j) = xMatrix(i,j)
			enddo
		enddo
		
	end subroutine DiffFind
	
	subroutine FDM(xMatrix,xMatrixOld,bMatrix,column_start,column_end,columns_glo,rank,nprocs,dx,dy, rows,nx,ny)
		implicit none
		
		integer											:: nx,ny
		integer											:: i,j
		integer, intent(in)								:: column_end, column_start, columns_glo, rows, rank, nprocs
		double precision, dimension(nx,ny)				:: bMatrix,xMatrix,xMatrixOld
		double precision, intent(in)					:: dx,dy
		
		if (rank.eq.0 .and. nprocs.eq.1) then
			do j = 2,columns_glo-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		elseif (rank.eq.0 .and. nprocs.ne.1) then
			do j = column_start+1,column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		elseif (rank.eq.nprocs-1) then
			do j = column_start,column_end-1
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		else
			do j = column_start, column_end
				do i = 2,rows-1
					xMatrix(i,j) = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + &
											xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + &
											bMatrix(i,j)* dx * dy)
				enddo
			enddo
		endif
	
	end subroutine FDM
	
	subroutine Setup_Broadcast(nx, ny, CBC, CSOL, dx, dy, IFlag, eps, maxIter)
	
		implicit none
		include 'mpif.h'
		integer											:: nx, ny, CBC, CSOL, IFlag, ierr, maxIter
		double precision								:: dx, dy, eps
		
		call MPI_Bcast(nx,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(ny,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(CBC,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(CSOL,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(IFlag,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(dx,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(dy,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(eps,1,MPI_Double_Precision,0,MPI_COMM_WORLD,ierr)
		call MPI_Bcast(maxIter,1,MPI_Integer,0,MPI_COMM_WORLD,ierr)
		
		call MPI_Barrier(MPI_COMM_WORLD,ierr)
		
	end subroutine Setup_Broadcast
	
	subroutine Setup_Matrix(xMatrix,xMatrixOld,bMatrix,columns_glo,rows)
	
		implicit none
		
		integer											:: rows, columns_glo
		double precision, dimension(:,:), allocatable	:: bMatrix, xMatrix, xMatrixOld
		
		allocate(xMatrix(rows,columns_glo),bMatrix(rows,columns_glo),xMatrixOld(rows,columns_glo))
		
		xMatrix = 0.0
		xMatrixOld = 0.0
		bMatrix = 0.0
		bMatrix(rows/2,columns_glo/2) = 1.0
	
	end subroutine Setup_Matrix
	
	subroutine Setup_General(columns_glo,rows,columns_loc,column_start,column_end,nx,ny,nprocs,rank)
	
		implicit none
		include 'mpif.h'
		integer											:: nx, ny, rows, columns_glo, columns_loc
		integer											:: rank, nprocs, column_end, column_start
		
		columns_glo = ny
		rows = nx
		columns_loc = (columns_glo)/nprocs
		
		if (rank.eq.0) then
			column_start = (rank) * columns_loc + 1
			column_end = (rank+1) * columns_loc
		else
			column_start = (rank) * columns_loc + 1
			column_end = (rank+1) * columns_loc
		endif
		
		if (rank.eq.nprocs-1 .and. nprocs.gt.1) then 
			column_end = columns_glo
		endif
		
	end subroutine Setup_General
	
	subroutine Read_Input_File(nx, ny, CBC, CSOL, dx, dy, IFlag, maxIter)
		implicit none
		
		integer, intent(out)							:: nx, ny, cbc, csol, IFlag, maxIter
		real*8, intent(out)								:: dx, dy
		integer											:: ierror, i_value
		integer											:: comment_pos, equal_pos
		real											:: r_value
		character*80									:: line, inputfile, tag, comment
		
		inputfile = 'input.in'
		Open(unit=14,file=trim(inputfile),status='old',action='read')
		
		do
			read(14, '(a)',iostat=ierror) line
				if (ierror.ne.0) then
					exit
				endif
			comment_pos = index(line,'#')
			
			if (comment_pos.eq.1) then
			else
				read(line,*,iostat=ierror) tag
				select case (tag)
					case ('NX')
						read(line,*,iostat=ierror) tag, r_value, comment
						nx = int(r_value)
					case ('NY')
						read(line,*,iostat=ierror) tag, r_value, comment
						ny = int(r_value)
					case ('CBC')
						read(line,*,iostat=ierror) tag, r_value, comment
						cbc = int(r_value)
					case ('CSOL')
						read(line,*,iostat=ierror) tag, r_value, comment
						csol = int(r_value)
					case ('DX')
						read(line,*,iostat=ierror) tag, r_value, comment
						dx = r_value
					case ('DY')
						read(line,*,iostat=ierror) tag, r_value, comment
						dy = r_value
					case ('IFLAG')
						read(line,*,iostat=ierror) tag, r_value, comment
						IFlag = int(r_value)
					case ('MAXITER')
						read(line,*,iostat=ierror) tag, r_value, comment
						maxIter = int(r_value)
				end select
			endif
		enddo
		close(14)
		
	end subroutine Read_Input_File
end module ParallelMod