#CSE597
#Author: Jacob Zorn (jzz164@psu.edu)
#Homework0
#Make File for Zorn_Hello.f90

FC=ifort

SOURCES=Zorn_Hello.f90
OBJECTS=$(SOURCES:.f90=.o)
EXECUTABLE=Zorn_Hello.out

all: Zorn_Hello.out

Zorn_Hello.out: Zorn_Hello.f90
	$(FC) Zorn_Hello.f90 -o Zorn_Hello.out

clean:
	rm *o Zorn_Hello.out
