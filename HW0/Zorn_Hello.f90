!CSE597 - Implementation of Parallel Computing Codes
!Author: Jacob Zorn
!Program Name: Hello World
!Filename: Zorn_Hello.f90
!Description---------------------------
!This program when run will print the following to console/terminal:
! jzz164 says "Hello I Hope You Have a Great Day!"
!End Description-----------------------
!See the accompanying ReadMe and MakeFile for compliation instructions

!Program Start
program hello_world
	print*, 'jzz164 says "Hello I Hope You Have a Great Day!'
end program hello_world