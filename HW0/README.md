# CSE_597 HW0

#Basic Information

Author: Jacob Zorn
Date: Aug 24, 2018

The licensing information is in the attached license.txt file.

HW0

#Compilation instructions for ACI
module load intel

make

./Zorn_Hello.out

#Expected Output

jzz164 says "Hello I Hope You Have a Great Day!"

#To Compile the Write Up
./pdfmake.sh