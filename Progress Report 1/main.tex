\documentclass{article}
% Change "article" to "report" to get rid of page number on title page
\usepackage{amsmath,amsfonts,amsthm,amssymb}
\usepackage{setspace}
\usepackage{Tabbing}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{extramarks}
\usepackage{url}
\usepackage{chngpage}
\usepackage{longtable}
\usepackage{soul,color}
\usepackage{graphicx,float,wrapfig}
\usepackage{enumitem}
\usepackage{morefloats}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{indentfirst}
\usepackage{lscape}
\usepackage{pdflscape}
\usepackage{natbib}
\usepackage[toc,page]{appendix}
\providecommand{\e}[1]{\ensuremath{\times 10^{#1} \times}}

% In case you need to adjust margins:
\topmargin=-0.45in      % used for overleaf
%\topmargin=0.25in      % used for mac
\evensidemargin=0in     %
\oddsidemargin=0in      %
\textwidth=6.5in        %
%\textheight=9.75in       % used for mac
\textheight=9.25in       % used for overleaf
\headsep=0.25in         %

% Homework Specific Information
\newcommand{\hmwkTitle}{Progress Report 1}
\newcommand{\hmwkDueDate}{Monday,\ September\  24,\ 2018}
\newcommand{\hmwkClass}{Final Project}
\newcommand{\hmwkClassTime}{CSE 597}
\newcommand{\hmwkAuthorName}{Jacob\ A.\ Zorn}
\newcommand{\hmwkNames}{jzz164}

% Setup the header and footer
\pagestyle{fancy}
\lhead{\hmwkNames}
\rhead{\hmwkClassTime: \hmwkTitle} 
\cfoot{Page\ \thepage\ of\ \pageref{LastPage}}
\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make title

\title{\vspace{2in}\textmd{\textbf{\hmwkClass:\ \hmwkTitle}} \\
\vspace{0.1in}\large{ \hmwkClassTime}\vspace{3in}}

\author{\textbf{\hmwkAuthorName} \\ \vspace{0.1in}
\hmwkDueDate }
\date{} % to take away today's date

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\begin{spacing}{1.1}
\maketitle

\newpage
\section*{Abstract}

The electrostatic free energy contribution is extremely important in understanding domain structures and their development in ferroelectric materials. In order to derive this contribution, it becomes necessary to solve Poisson's equation at each time step. Therefore, the development of a fast, stable, and accurate solver is necessary to understand the evolution of domain structures. In this paper, we compare the performance of a direct solving approach, based on LU-Decomposition, and an iterative solving approach, based on Jacobi Relaxation. It was found that Jacobi Relaxation was ~4000x faster on problems of 100x100 domain sizes for our test problems.

\section{Problem of Interest}

\begin{itemize}
    \item What is the problem?
    \begin{itemize}
      \item The problem we are discussing here is Poisson's equation for describing the electric potential, and therefore electric field, in a material. Poisson's equation can be seen below in (1), \\
      \begin{equation} \label{eq1}
      \Delta U = f(x,y).
      \end{equation} \\
      Understanding the distribution of the electric field is necessary for proper simulation of ferroelectric thin film systems in order to correctly approximate the free energy contribution of an electric field. This contribution leads to the development of the common head-to-tail domain structures that exist in materials such as lead titanate (PTO), lead titanate zirconate (PZT), and barium titanate (BTO). Therefore, allowing for correct simulation and prediction of the domain structures that may appear in not only those systems but also other ferroelectric material systems. Not only is it imperative to correctly approximate the electric field in ferroelectric and other electronic materials, but also in the modeling and simulation of magnetic materials.
    \end{itemize}
    \item General overview
    \begin{itemize}
    	\item A ferroelectric domain structure or state is traditionally defined as a spatial distribution of regions of aligned electric dipoles, separated by domains across which the polarization of the dipoles changes\citet{moulson}. These domain structures or states change in orientation and polarization as to minimize the total free energy of the system. The free energy of these systems are dependent on a number of factors such as, including but not limited to, chemical free energy (think chemical bonding or elemental composition), elastic or mechanical free energy, and electrostatic energy. The sum of these free energy contributions create what we term a \textit{Free Energy Functional} and can be described by (2) below,\\
        \begin{equation} \label{eq2}
        	\int_V f_{chemical}(P_{i}) + f_{elastic}(P_{i},\eta_{ij}) + f_{electric}(P_{i},E_{i})  dV = F(P_{i},\eta_{ij},E_{i}).
        \end{equation}
        \item This free energy is necessary for the various evolution equations that are utilized to model microstructural evolution of materials. The Allen-Cahn Equation (3) and Cahn-Hillard Equation (4) are the two equations used to simulate this processes. Here \textit{Q} and \textit{R} are order parameters that characterize some phase of the material. More information regarding solving methods and applications of these equations can be found in \citet{biner2017,chenreview}.
        \begin{equation} \label{eq3}
        	\frac{\partial Q}{\partial t} = -L \frac{\delta F}{\delta Q}
        \end{equation}
        \begin{equation} \label{eq4}
        	\frac{\partial Q}{\partial t} = \nabla M \nabla \frac{\delta F}{\delta R}
        \end{equation}
      	\item Therefore, we can see it is necessary to fully understand the electrostatic potential and the electric field that can develop due to these dipoles in a material system. Thus an accurate solution to Poisson's equation is necessary to fully understand this microstructural evolution.
    \end{itemize}
    \item Scientific merit (Why is this worth doing?)
    \begin{itemize}
    	\item Poisson's Equation is necessary to solve because so many different physical processes can be described by Poisson's equation or the analogous Laplace equation. With applications ranging from magneto-static and electrostatic equilibrium to heat transfer and beyond, it is widely known that Poisson's equation has a range of relevant fields. Additionally, this is also necessary for understanding the electric contribution to the free energy of a material system so the domain structure can be modeled properly.
    \end{itemize}
    \item Fields where relevant
    \begin{itemize}
		\item Poisson's equation shows up in a variety of scientific and engineering fields. Not only do material engineers and scientists use Poisson's equation to derive electric field but computational fluid dynamics modelers also use Poisson's equation. The homogeneous form of the Poisson's equation, where the right hand side is equal to zero, is commonly referred to as the Laplace equation. The Laplace equation is used predominately in the field of thermal modeling and steady state heat transfer.
    \end{itemize}
    \item What other methods are used to solve this type of problem?
    	\begin{itemize}
        	\item There are a variety of ways of solving Poisson's equation, and they are all dependent on the boundary conditions imposed on the system in question. For example, as will be discussed a parallel plate model of Poisson's equation allows the use of Successive Over-Relaxation (SOR) whereas a singular charge or dipole charge does not allow for the use of Successive Over-Relaxation. Therefore, in cases where SOR can't be utilized a better more stable method must be used, such as Jacobi relaxation. Of course, there will always exist a solution can be derived directly, such as matrix inversion or LU decomposition, as we use here to solve the problem directly. Analytically solutions also exist and are commonly found in Fourier space through the use of Fourier Transforms. A common solution utilized in the phase field modeling of ferroelectric domain structures is described by Li \textit{et al.} \citet{Li2002,Li2004}.
        \end{itemize}
    \item Discussion of known solution (analytic/published)
    	\begin{itemize}
        	\item The solution will be replicating in this paper comes from the work of Ian Cooper at the University of Sydney. We will replicating the effect of a single charge at the origin of a two dimensional plate, thus solving Poisson's equation in two dimensions\citet{Cooper}. We wish to understand the electric potential and, by appropriate derivation, the electric field. We can then utilize the electric field we derive to determine the electrostatic contribution to the free energy of a Ferroelectric system. Note that we do not calculate this contribution and this program would only be used as an additional module or library to model the microstructural evolution. We can see from the work of Cooper, and of this work, that while the point charge at the origin is infinitely steep, the potential that is derived from that has a defined slope which gives rise to an electric field, which permeates through the entire plate.
        \end{itemize}
\end{itemize}

\subsection{Numerical Set-up}

\begin{itemize}
    \item How the A-matrix and b-vector are formed
    \begin{itemize}
    	\item The A-matrix is a matrix of size \(nx \times ny\) by \(nx \times ny\) where nx and ny are the number of elements in the x and y directions respectively. Thus, every element (or point) is represented in each row of the A-matrix. This is done through transforming each point in the real matrix system $A_{i,j}$ to some common in each row of the A-matrix. Therefore to accomplish this we cycle through the elements and define each element in the A-matrix as, $A_{n,(i-1)ny + j}$, where i is the row of the element in question in the spatial A-matrix and j is the column of the element in question in the spatial A-matrix, and n is just the row of the A-matrix we are deriving. Through this transformation we can quickly and easily define the A-matrix necessary for solving Poisson's equation.
        \item The b-vector is formed in a very similar manner but since there is only one column we can transform a b spatial matrix to a b-vector by the following transformation,\\
        \begin{equation} \label{eq2}
      	b_{i,j} = b_{(i-1)ny + j,1}.
      	\end{equation} \\
        Where i, ny, and j all have the same meanings as deriving the A-matrix.
    \end{itemize}
    \item Problem sizes for problems of interest, test problems
    \begin{itemize}
    	\item The problem sizes for the problems of interest will be of three dimensions and most commonly of size 128x128x36 or similar sizes. This allows us to describe the evolution of ferroelectric domain structures in a way that is applicable to real life experiments. When running test problems, we consider only the two dimension case and consider the case of a constant potential at the center of the map.
    \end{itemize}
    \item Boundary conditions (if applicable)
    \begin{itemize}
    	\item There are a few different boundary conditions that can be applied to this problem. The most commonly applied boundary condition are Dirichlet Boundary Conditions, which are boundary conditions where \(Z(x,y) = f(x,y)\) meaning that any location on the boundary is defined by some given function or constant. In the case of this work, we simply allow \(Z(x,y) = 1\) on the boundaries.The second type of boundary condition applied in this work are Neumann Boundary Conditions, which are conditions under which the flux of the boundaries is equal to some predefined function, such as \(\delta(Z(x,y)) = f(x,y)\). This has a variety of applications because it can lead to the creation of open or closed systems, whereas material or charge can enter or exit the system. Conversely, the flux can be set to zero to allow for zero-flux boundary conditions which are highly used in diffusion and electric charge simulations.\\
    \\
	The third type of boundary condition utilized are periodic or cyclic boundary conditions. These types of boundary conditions are helpful to systems at can be considered periodic in nature, such as \(Z(x) = Z(x+2\pi)\). These types of boundary conditions are popular in material science simulations, owing to the periodicity of the underlying crystal structure. Other fields that utilize periodic boundary conditions in their simulations and research are fluid dynamics \citet{Hirt}
    \\
	Lastly, we can take any of the aforementioned boundary conditions and combine them to make what are called Mixed Boundary Conditions. The introduction of these mixed boundary conditions allow for solutions that more closely mimic reality. For extreme, a common boundary condition that is applied to solving Poisson's equation consider two parallel plates held at different potentials connected by some dielectric block where no field can leak out. This leads to the introduction of mixed Dirichlet and Neumann boundary conditions. It is easily seen that Neumann-Zero Flux boundary conditions are imposed on the sides of the dielectric block whereas Dirichlet boundary conditions are considered for the top and bottom of the dielectric block. Other types of mixed boundary conditions exist, for example one can mix periodic and Dirichlet boundary conditions where the underlying physics of the system as periodic in one or two directions yet not periodic in a third.
    	\item In the problems of interest, it is necessary to consider mixed boundary conditions. For example, since we are attempting to model a crystalline system in three dimensions, we need to consider the case of boundary conditions where the x and y components are periodic, but the z component or direction is either Dirichlet or Neumann boundary conditions. Commonly, we will consider Dirichlet boundary conditions so we can impose some form of electrostatic potential.
        \item However, for the test problem, we will only consider the case of Dirichlet boundary conditions on the edges of the system, as this matches up well with common test problems we can compare our solution to.
    \end{itemize}
    \item Discretization method (if applicable)
    \begin{itemize}
    	\item In this program, we do not use any abnormal discretization method and instead rely on the normal 5-point Finite Difference Method Stencil to solve Poisson's Equation.
    \end{itemize}
\end{itemize}

\section{Solvers}

\subsection{Direct Solver}

\begin{itemize}
    \item Direct solver being used 
		\begin{itemize}
			\item I chose to use LU decomposition as the direct solving method for this project.
		\end{itemize}
    \item Justification for direct solver being used
		\begin{itemize}
			\item The reason that LU decomposition was used is because after the LU decomposition has been completed there is no reason to derive the LU decomposition once again. Therefore, since in production runs it is imperative to calculate the electric field and the potential at every time step, not having to derive a large LU decomposition will be extremely helpful. Additionally, the forward and backward substitution when solving for 'x' is very quick and won't be extremely costly to total production time.
		\end{itemize}
    \item List of and justification for optimization flags 
	\begin{itemize}
    	\item For this project, we are simply using the built-in optimization flags for the GNU and PGI compilers, as we studied the performance differences between the two. Thus we have considered the flags -O0, -O1, -O2, -O3, and -O4. It was found that with the GNU compiler that the -O1 flag was the best of the optimization flags, but when compiled with PGI the -O4 flag shows a very large amount of speedup that is very helpful for the derivation of the LU matrices.
    \end{itemize}
    \item Timing for test problem 
    \begin{itemize}
		\item The plot below showcases the timing of number of elements in the A-matrix for solving Poisson's equation. It can be easily seen that it scales with $n^{2}$, where $n$ is the number of elements in the A-matrix. It takes over 4300 seconds to solve a 10000x10000 matrix via LU decomposition. \\
        \begin{figure}
        \begin{center}
        \includegraphics[width=0.6\linewidth]{DirectSolvingTime.png}
        \caption{Time to Directly Solve a Matrix of 10,000 - 100,000,000 elements. It is obvious that the scaling is approximately $n^{2}$.}
        \label{fig:Fig1}
        \end{center}
        \end{figure}
        \item However, if we compile with a different compiler, in this instance we used the PGI set of Fortran compilers, we can achieve a speedup of ~10x and solve the test problem in ~392 seconds. Thus demonstrating that the choice of compiler can have a drastic effect on program performance.
    \end{itemize}
    \item Projection of time for production problem
    \begin{itemize}
		\item If we extrapolate out from $10^{8}$ elements for the test problem out to the production problem containing ~$3.48 \times 10^{11}$ elements we can estimate the timing for decomposing the A-matrix into the appropriate LU matrices to ~$5.04 \times 10^{10}$ seconds. The backward and forward substitution are relatively quick compared to the decomposition, and thus we don't comment on it here.
    \end{itemize}
    \item Memory being allocated for test problem
		The memory allocated for the largest of the test problems (100x100 real space matrix) results in an A-matrix of size (10000x10000). Since the spatial steps (dx and dy) are real values we must assign our A-matrix to be real values as well instead of integers which means we must use more memory. Additionally, since no kind of sparse matrix decomposition was utilized we still must keep all the zeros are populate the A-matrix. These results in a matrix of 100,000,000 8-byte elements. Results in the A-matrix of approximately 800 megabytes (MB). Since we are also deriving L and U matrices, we would need two additional matrices of size 10000x10000 and thus another 1600 MB. The x-vector and b-vector are both of size 1x10000, and thus only consist of 10,000 8-byte real elements. Therefore, each of those vectors have a total memory allocation of approximately 0.080 MB or 80 kilobytes (KB). Providing a total memory allocation of ~2400.160 MB for running the test problem. 
    \item Comparison with memory being used by task
	\begin{itemize}
    	\item When comparing the memory usage of allocated to actual used memory, determine via the "/usr/bin/time -v" command, we see that we use approximately the same amount of memory as allocated to solve this problem. The "/usr/bin/time -v" command tells that we utilize ~2,350,220 KB which evaluates to ~2295 MB which is close to our calculated memory allocation of ~2400 MB.
    \end{itemize}
    \item Projection of memory required for production problem
		Since this problem would need to be solved in three dimensions to be used applicably in the simulation of ferroelectric domain structures, we can safely assume the memory usage will be at least an order of magnitude greater. For example, if we consider a simulation of a ferroelectric thin film and the simulation cell is 128x128x36 nanometers (nm), and for simplicity we take dx=dy=dz=1 nm, then our A-matrix will become a 589,824x589,824 matrix. Which will require an allocation of approximately 278 gigabytes (GB) of memory, plus two additional matrices of size 589,824x589,824 to accommodate the L and U matrices and another ~556 GB. Much to larger for any one processor to handle. Whereas, the x and b vectors would only need approximately 4.70 MB of memory. Thus it can be seen that we need almost one terabyte of memory in order to solve Poisson's equation directly.
\end{itemize}

\subsection{Iterative Solver}

\begin{itemize}
    \item Iterative solver being used
		\begin{itemize}
			\item I have chosen to solve Poisson's equation iteratively using the Jacobi method. While Successive Over-Relaxation can be used for Poisson's equation, it is widely necessary to consider the initial conditions prior to selecting the solving method. For example, if we only consider a charge at the origin of the system, then only a stable Jacobi method will work for convergence. However, if we consider a parallel plate set up then a scheme such as Over-Relaxation will work to complete the task.
		\end{itemize}
    \item Justification for iterative solver being used
		\begin{itemize}
			\item Jacobi relaxation was chosen because of the stable and guaranteed convergence of the system. However, as discussed previously, using different initial conditions can lead to using different solving methods or relaxation factors. Additionally, different boundary conditions can lead to using different relaxation methods.
        \end{itemize}
    \item Justification for convergence criteria, and residual/norm being used
	\begin{itemize}
    	\item The convergence criteria I used for this solving method was a global maximum error. Thus at every iteration we calculate the difference between the current iteration and previous iteration and only when the global maximum error is below a tolerance value does the iteration stop. This method of convergence is utilized because when determining the electric free energy contribution to the free energy of the system the entire system must be correct. Thus in order to ensure it is correct, the global error must be minimized and this method of error convergence is the best way to ensure that.
    \end{itemize}
    \item List of and justification for optimization flags
    \begin{itemize}
    	\item In this project, we again relied on the default optimization flags provided by the various compilers we used in this project, GNU and PGI. We cycled through the following optimization flags on both GNU and PGI: -O0 -O1 -O2 -O3 -O4. It was found that the -O4 optimization flags with both compilers performed the best. Additionally, it was possible to see that there was a very small difference between the two compilers when using an iteratively solving method. This is very different than what we see when solving with a direct method.
    \end{itemize}
    \item Timing for test problem for 3 different initialization methods (random, good guess, all zeros or ones)
	\begin{itemize}
    	\item Five different initialization methods were programmed into this codes: all 1's, all 0's, random, b-matrix values, and random values between 0.1 and 0.2. As we cycle through these methods, we see that the initialization routine that is based off of the original b-matrix is the fastest, followed by all 1's and all 0's. The completely random initialization is the slowest and the random values between 0.1 and 0.2 is only slightly slower than the previously discussed methods. There is only ~10\% change between the slowest and fastest methods of solving.\\
        \begin{figure}
        \begin{center}
        \includegraphics[width=0.6\linewidth]{InitalizationRoutine.png}
        \caption{Iterations to solve Poisson's equation dependent on various initializations of the x-matrix or vector. It can be seen that aside from the random number between 0 and 1, that all of the other routines are approximately the same number of iterations to achieve convergence.}
        \label{fig:Fig2}
        \end{center}
        \end{figure}
    \end{itemize}
    \item Plot showing convergence, comparison of convergence rate with expected rate
	\begin{itemize}
    	\item Now we know that the convergence of a Jacobi method is quite slow when compared to other methods, such as Gauss-Steidel and Over-Relaxation techniques. Therefore, we expect a convergence rate of 1, such that Jacobi is the "standard" for solving the equation. As we show in Figure 3 below, if we switch to another method, Gauss-Steidel for example, our rate of convergence is nearly double that of the Jacobi Method.
        \begin{figure}
        \begin{center}
        \includegraphics[width=0.6\linewidth]{Converge.png}
        \caption{The Difference in Convergence speed between normal Jacobi Relaxation and Gauss-Steidel relaxation. It is easily apparent that Gauss-Steidel is much about twice as fast Jacobi relaxation, which is to be expected.}
        \label{fig:Fig3}
        \end{center}
        \end{figure}
    \end{itemize}
    \item Projection of time required for production problem
	\begin{itemize}
    	\item As we can see from Figure 4 below, the iterative solution used here scales with respect to $n^{2}$. If we extend this to a production problem, then we can estimate that for the time for a production problem would be in the range of ~280-290 seconds.
        \begin{figure}
        \begin{center}
        \includegraphics[width=0.6\linewidth]{IterativeSolvingTime.png}
        \caption{Once again, we can see that the solving time scales with approximately $n^{2}$, however, since we don't have to derive LU matrices, the solving time is greatly reduced.}
        \label{fig:Fig3}
        \end{center}
        \end{figure}
    \end{itemize}
    \item Memory being allocated for test problem
    	\begin{itemize}
		\item The largest test problem that would be run by this code would be a 100x100 matrix, thus only have 10,000 8 byte real elements. Thus, only ~80 KB of memory would need to be allocated to the solution of this problem. When we include the b-vector, which becomes a matrix in the case of a iterative solution, this again only contains 10,000 8 byte real elements and only would need approximately 80 KB of memory. Therefore, we would need approximately ~160 KB of memory to store the necessary matrices. We will consider the naming schemes and allocation of memory in the code, we come up with an estimate of ~500 KB of memory necessary to solve the problem.
        \end{itemize}
    \item Comparison with memory being used by task
    \begin{itemize}
		\item In the same way we derived the memory usage in the case of the direct solving method, we use the "/usr/bin/time -v" command to understand the memory usage. In a 100x100 example, we have a peak memory usage of ~1000 KB which is approximately double the predicted memory usage. I believe we see this effect due to the the row and column indexing and potentially using row-major versus column-major indexing thus causing a higher memory usage.
    \end{itemize}
    \item Projection of memory required for production problem
    \begin{itemize}
		\item Once again, we consider the case of a 128x128x36 nm ferroelectric thin film, with equal spatial discretization steps. Therefore, if we construct these 3D matrices for both the x-vector (which becomes a matrix) and the b-vector (which again becomes a matrix in the iterative solving routine), we arrive at two matrices of 589,824 8 byte real elements. Each of which are allocated approximately 4.75 MB. Thus we would only need approximately 5 MB of memory to run a production scale problem using an iterative solving approach, which is only 0.003\% of the memory necessary to solve the same problem directly.
   \end{itemize}
\end{itemize}

\section{Solver Comparison}

\begin{itemize}
    \item Which solver does better? (compute time, memory)
    \begin{itemize}
		\item The iterative solver is much better for solving both the test problem and the production problems. Not only does it use drastically less memory, but the solving time is orders of magnitude less. This is widely helpful for the development of fast code that is necessary to solve the tens of thousands of time steps necessary to understand the development of ferroelectric domain structures.
    \end{itemize}
    \item Which solver will do better for a production scale problem?
    \begin{itemize}
		\item Based on the data gathered from the test problems, we can see that the iteratively solving method will be much better for a production scale problem. In terms of both memory usage and computation time.
    \end{itemize}
    \item Discuss how the production problem projections will be constrained and what will need to be taken into account for parallelization
    \begin{itemize}
		\item For a production problem, more solving routines will need to be introduced. This is account for the various Relaxation techniques that will can be utilized. Additionally, the correct row-major or column-major indexing will be need to taken into account as well in order to ensure the not use the memory incorrectly.
    \end{itemize}
\end{itemize}

\section{Discussion and Conclusions}

\begin{enumerate}
    \item Basic overview of what you have done
    \begin{itemize}
    	\item A Poisson's equation solver has been developed, that solves Poisson's equation both directly and iteratively. To solve Poisson's Equation directly, the LU decomposition technique is utilized. To solve Poisson's Equation iteratively, Jacobi relaxation was utilized. It is shown that Jacobi relaxation is ~3.5 orders of magnitudes faster to finding an appropriate solution. 
    \end{itemize}
    \item Justification for solver choices 
    \begin{itemize}
		\item LU Decomposition was utilized due to the quickness of LU Forward and Backward Substitution. Once the LU Decomposition is complete and the L and U matrices are derived, then the Forward and Backward substitution is quite rapid. Since the L and U matrices will not change from time step to time step and only the RHS changes, then using just Forward and Backward substitution, the different LHS can be solved for quickly, which is a key component of the necessary research into Ferroelectric Domain Structures.
        \item Jacobi relaxation was utilized because it is stable regardless of the boundary or inital conditions. In future iterations of this code, it will be necessary to adjust the solving method based on the initial conditions. As discussed previously, a parallel plate model will be solvable using Over-Relaxation while a single charge at the origin is only solvable through Jacobi Relaxation. 
    \end{itemize}
    \item Discussion of constraints of production problem with requirements for parallelization
    \begin{itemize}
		\item Some constraints that will pop up when solving the production problem with parallelization, will be proper allocation of memory. As discussed previously, almost one terabyte of memory will be necessary to derive the LU matrices and over 500 GB of memory will be necessary to store the LU matrices. Therefore, during parallelization it will be necessary handle that memory correctly in order to arrive at the correct answer.
    \end{itemize}
\end{enumerate}


\newpage
\begin{appendices}

\section{Code}

\begin{itemize}
    \item Where can you find the 
    \begin{itemize}
		\item The code can be found on Gitlab by copy and pasting this url or through proper git cloning: $https://gitlab.com/jzorn164/CSE_597$
    \end{itemize}
    \item File names and descriptions
	\item There are 3 main files that are necessary to run this program. They are listed below with a brief description of each file.
	\begin{itemize}
		\item \textbf{DirectSolving.f90}
        \begin{itemize}
			\item This file is a Fortran Module which contains all of the necessary subroutines for the program to run successfully. Inside this file are subroutines for the LU Decomposition of the A-matrix, Successive Over-Relaxation routines, along with subroutines to build various matrices, set boundary conditions, and read the input file.
		\end{itemize}
        \item \textbf{Direct.f90}
        \begin{itemize}
			\item This file is a Fortran program that runs the Poisson Equation solver. It makes a variety of subroutine calls and allocates the necessary memory. 
		\end{itemize}
        \item \textbf{input.in}
        \begin{itemize}
		\item This is an input file that must be edited by the user prior to running the solver. This includes information related to system size, spatial step size, type of solver, and type of boundary conditions.
		\end{itemize}
    \end{itemize}
    \item Instructions for compiling and reproducing your results
    \begin{itemize}
	\item The program can be compiled by use the provided Makefile. Simply type "make" into your terminal to compile the code with the necessary optimization flags. After which, you can run the code by typing "./PoiSolver.exe". The provided input file will mimic the results that are provided in this write-up.
    \end{itemize}
    \item Listing of which nodes types you ran on
\end{itemize}

\section{Licensing and Publishing}

\begin{itemize}
    \item Discussion of license choice
    \begin{itemize}
		\item I chose to use the GNU General Public License (GPL) as my license of source, so my code is as open and accessible as possible. The General Public License offers individuals the freedom to modify, run, and utilize the code as they see fit. Additionally, this license choice also requires anyone that modifies the code and decides to distribute it, must do so under the same license. This can lead to better and faster solving routines that be utilized in the future, based off of derivatives of this work.
    \end{itemize}
    \item Discussion of location of publication
    \begin{itemize}
	\item Gitlab was utilized as the location of publication due to my use of Gitlab for a variety of projects, including most of the source code that we use in my research group. Additionally, since I already had an account on Gitlab and not on another site, say Github, it just made more sense to utilize my current resources in terms of repositories.
    \end{itemize}
\end{itemize}
\end{appendices}

\bibliographystyle{acm}
\bibliography{sample}

\end{spacing}
\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%}}
