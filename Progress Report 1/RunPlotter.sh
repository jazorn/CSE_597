#! /bin/bash

#Simple Script to Run Matlab Plotter Script for Poisson Equation Solver
#Written by Jacob Zorn
#CSE597
#Written: 9/19/2018


module load matlab
matlab -nodisplay -nosplash < Plotter.m > matlab_out.log