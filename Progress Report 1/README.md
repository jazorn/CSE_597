# CSE_597 Progress Report 1
#Basic Information

Author: Jacob Zorn
Date: Sep 16, 2018

The licensing information is in the attached license.txt file and is licensed under the GNU General Public License.

This program is a Poisson's Equation solver with Drichlet Boundary Conditions, Periodic Boundary Conditions, and Neumann Boundary Conditions. This code is only serial and thus is relatively slow. All of these boundary conditions can be implemented and solved both directly and iteratively. More information is available in the write-up PDF.

#Compilation instructions for ACI

Note: For maximum performance, it is recommended to compile using PGI compilers with various optimization flags. However, the accompanying MakeFile uses the GNU family of Fortran compilers.

Make sure to read through the accompanying documentation and update the input.in file prior to running the program.

make
./PoiSolver.exe

#Expected Output
Produces a xVec.dat file. Running the accompanying Matlab file (Plotter.m) will produce a PNG image of the results. The Matlab file has been packaged inside a shell script to make running it easier if necessary. Just make sure to make the shell script executable using the appropriate CHMOD command.

#To Compile the LaTex Progress Report

./ReportMake.sh


