! Fortran Module for Direct Solving of Ax=b problems
! Written for CSE597
! Pennsylvania State University
! 9/11/2018
! Jacob Zorn

module DirectSolving
	implicit none
	
	contains
	
	subroutine Read_Input(nx, ny, CBC, CSOL, dx, dy, InitalFlag)
		implicit none
		
		integer, intent(out)							:: nx, ny, cbc, csol, InitalFlag
		real*8, intent(out)								:: dx, dy
		integer											:: ierror, i_value
		integer											:: comment_pos, equal_pos
		real											:: r_value
		character*80									:: line, inputfile, tag, comment
		
		inputfile = 'input.in'
		Open(unit=14,file=trim(inputfile),status='old',action='read')
		
		do
			read(14, '(a)',iostat=ierror) line
				if (ierror.ne.0) then
					exit
				endif
			comment_pos = index(line,'#')
			
			if (comment_pos.eq.1) then
			else
				read(line,*,iostat=ierror) tag
				select case (tag)
					case ('NX')
						read(line,*,iostat=ierror) tag, r_value, comment
						nx = int(r_value)
					case ('NY')
						read(line,*,iostat=ierror) tag, r_value, comment
						ny = int(r_value)
					case ('CBC')
						read(line,*,iostat=ierror) tag, r_value, comment
						cbc = int(r_value)
					case ('CSOL')
						read(line,*,iostat=ierror) tag, r_value, comment
						csol = int(r_value)
					case ('DX')
						read(line,*,iostat=ierror) tag, r_value, comment
						dx = r_value
					case ('DY')
						read(line,*,iostat=ierror) tag, r_value, comment
						dy = r_value
					case ('IFLAG')
						read(line,*,iostat=ierror) tag, r_value, comment
						InitalFlag = int(r_value)
				end select
			endif
		enddo
		close(14)
		
	end subroutine Read_Input
	
	subroutine SetBoundaryConditions(Nx, Ny, Flag, Amatrix)
		implicit none
		
		real*8, dimension(:,:), intent(out)				:: Amatrix
		integer, intent(in)								:: Nx, Ny, Flag
		integer											:: i, j, nrow
		
		!Flag value for different boundary conditions
		!Flag = 1 for Dirichlet Boundary conditions (y = f)
		!Flag = 2 for Periodic Boundary Conditions in all directions
		!Flag = 4 for Periodic Boundary Conditions in the x direction not y direction
		!Flag = 3 for Neumann Boundary Conditions
		
		select case (Flag)
			case (1)
				!print*, "Solving Poisson's Equation with Dirichlet Boundary Conditions in all directions."
				call DirichletBoundaryConditions(Nx,Ny,Amatrix)
			case (2)
				!print*, "Solving Poisson's Equation with Periodic Boundary Conditions in all directions."
				call PeriodicBoundaryConditions(Nx,Ny, Amatrix)
			case (3)
				!print*, 'This type of boundary condition is not yet implemented. Will be implemented in next update.'
				! Periodic Boundary Conditions
			case (4)
				call NeumannBoundaryConditions(Nx, Ny, Amatrix)
			case (5)
				!print*, 'This type of boundary condition is not yet implemented. Will be implemented in next update.'
				!Mixed Neumann and Dirichlet Boundary Conditions
		end select 
	
	end subroutine SetBoundaryConditions
	
	subroutine PeriodicBoundaryConditions(Nx, Ny, Amatrix)
		implicit none
		
		real*8, dimension(:,:), intent(out)				:: Amatrix
		integer, intent(in)								:: Nx, Ny
		integer											:: i, j, nrow, nrowip, nrowim, nrowjm, nrowjp, n
		
		n = Nx * Ny
		
		do i = 1, Nx
			do j = 1, Ny
				nrow = (i-1)*Ny + j
				nrowip = (i)*Ny + j
				nrowim = (i-2)*Ny + j
				nrowjp = (i-1)*Ny + j + 1
				nrowjm = (i-1)*Ny + j - 1
				
				if (nrowjp.gt.n) then
					nrowip = nrowip - n
				endif
				
				if (nrowjp.gt.n) then
					nrowjp = nrowjp - n
				endif
				
				if (nrowim.le.0) then
					nrowim = nrowim + n
				endif
				
				if (nrowjm.le.0) then
					nrowjm = nrowjm + n
				endif
				
				Amatrix(nrow,nrow) = -4;
				Amatrix(nrow,nrowip) = 1;
				Amatrix(nrow,nrowim) = 1;
				Amatrix(nrow,nrowjm) = 1;
				Amatrix(nrow,nrowjp) = 1;
			enddo
		enddo
	
	end subroutine PeriodicBoundaryConditions
	
	subroutine NeumannBoundaryConditions(Nx, Ny, Amatrix)
		implicit none
		
		real*8, dimension(:,:), intent(out)				:: Amatrix
		integer, intent(in)								:: Nx, Ny
		integer											:: i, j, nrow
		
		do i = 1, Nx
			do j = 1, Ny
				nrow = (i-1)*Ny + j
				if (i.eq.1) then
					if (j.eq.1) then
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow+1) = 2
						Amatrix(nrow,nrow+Ny) = 2
					elseif (j.eq.Ny) then
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow-1) = 2
						Amatrix(nrow,nrow-Ny) = 2
					else
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow+1) = 2
						Amatrix(nrow,nrow+Ny) = 1
						Amatrix(nrow,nrow-Ny) = 1
					endif
				elseif (i.eq.Nx) then
					if (j.eq.1) then
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow+1) = 2
						Amatrix(nrow,nrow+Ny) = 2
					elseif (j.eq.Ny) then
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow-1) = 2
						Amatrix(nrow,nrow-Ny) = 2
					else
						Amatrix(nrow,nrow) = -4
						Amatrix(nrow,nrow+1) = 2
						Amatrix(nrow,nrow+Ny) = 1
						Amatrix(nrow,nrow-Ny) = 1
					endif
				else
					Amatrix(nrow,nrow) = -4
					Amatrix(nrow,nrow+1) = 1;
					Amatrix(nrow,nrow-1) = 1;
					Amatrix(nrow,nrow+Ny) = 1;
					Amatrix(nrow,nrow-Ny) = 1;
				endif
			enddo
		enddo
	
	
	end subroutine NeumannBoundaryConditions
	
	subroutine DirichletBoundaryConditions(Nx, Ny, Amatrix)
		implicit none
		
		real*8, dimension(:,:), intent(inout)				:: Amatrix
		integer, intent(in)								:: Nx, Ny
		integer											:: i, j, nrow, nxny
		
		nxny = Nx * Ny
		do i = 1, Nx
			do j = 1, Ny
				nrow = (i-1)*Ny + j
				if (i.eq.1 .or. i.eq.Nx) then
					Amatrix(nrow,nrow) = 1
				elseif (j.eq.1 .or. j.eq.Ny) then
					Amatrix(nrow,nrow) = 1
				else
					Amatrix(nrow,nrow) = -4
					Amatrix(nrow,nrow+1) = 1
					Amatrix(nrow,nrow-1) = 1
					Amatrix(nrow,nrow+Ny) = 1
					Amatrix(nrow,nrow-Ny) = 1
				endif
			enddo
		enddo
		
	end subroutine DirichletBoundaryConditions
	
	subroutine luDecom(Amatrix,Lmatrix,Umatrix)
		implicit none
		
		real*8, dimension(:,:)							:: Lmatrix, Umatrix, Amatrix
		integer											:: i, j, k, n,s 
		
		n = size(Amatrix,1);
		
		call Zero_Matrix(n,Lmatrix);
		call Zero_Matrix(n,Umatrix);
		
		do i = 1,n-1
			!Upper Triangular
			do k = i,n-1
				s = 0
				do j = 1, i-1
					s = s + Lmatrix(i,j) * Umatrix(j,k)
				enddo
				
				Umatrix(i,k) = Amatrix(i,k) - s
			enddo
			
			do k = i,n-1
				if (i.eq.k) then
					Lmatrix(i,i) = 1
				else
					s = 0
					do j = 1,i-1
						s = s + Lmatrix(k,j) * Umatrix(j,i)
					enddo
					
					Lmatrix(k,i) = (Amatrix(k,i) - s) / Umatrix(i,i)
				endif
			enddo
		enddo
	end subroutine luDecom
	
	subroutine LU_Decomp_NoPivot(Amatrix,Lmatrix,Umatrix)
	
		implicit none
		
		real*8, dimension(:,:)							:: Lmatrix, Umatrix, Amatrix
		integer											:: i,j,k,n
		
		!Calculate the size of the matrices
		n = size(Amatrix,1);
		
		!Create the start of the L matrix
		call Identity_Matrix(n,Lmatrix)
		
		!Create the start of the U matrix
		call Zero_Matrix(n,Umatrix)

		do k = 1,n
			Lmatrix(k+1:n,k) = Amatrix(k+1:n,k)/Amatrix(k,k)
			
			do i = k+1,n
				Amatrix(i,:) = Amatrix(i,:) - Lmatrix(i,k) * Amatrix(k,:)
			enddo
		enddo

		Umatrix = Amatrix;
		
	end subroutine LU_Decomp_NoPivot
	
	subroutine Identity_Matrix(MatrixSize,OutputMatrix)
		implicit none
		
		real*8, dimension(:,:)							:: OutputMatrix
		integer											:: MatrixSize, i, j
		
		do i = 1,MatrixSize
			do j = 1,MatrixSize
				if (i.eq.j) then
					OutputMatrix(i,j) = 1.0
				else
					OutputMatrix(i,j) = 0.0
				endif
			enddo
		enddo
	end subroutine Identity_Matrix
	
	subroutine Zero_Matrix(MatrixSize,OutputMatrix)
		implicit none
		
		real*8, dimension(:,:)							:: OutputMatrix
		integer											:: MatrixSize, i, j
		
		do i = 1,MatrixSize
			do j = 1,MatrixSize
				OutputMatrix(i,j) = 0.0
			enddo
		enddo
	end subroutine Zero_Matrix
	
	subroutine Write_Array(Array,newFile)
		implicit none
		
		real*8,dimension(:,:)								:: Array
		character*80									:: newFile
		integer											:: n, i, j
		
		n = size(Array,1)
		
		open(unit=13,file=trim(newFile),action='write',status='replace')
		do i = 1,n
			do j = 1,n
				write(13,"(10F10.2)",advance='no') Array(i,j)
			enddo
			write(13,*) ''
		enddo
		close(unit=13)
	
	end subroutine
	
	subroutine Write_Vector(Vector,newFile)
		implicit none
		
		real*8, dimension(:)								:: Vector
		character*80									:: newFile
		integer											:: n, i
		
		n = size(Vector,1)
		
		open(unit=13,file=trim(newFile),action='write',status='replace')
		do i = 1,n
			write(13,"(10F10.2)") Vector(i)
		enddo
		close(unit=13)
	end subroutine Write_Vector
	
	subroutine Forward_Sub(bVec,Lmatrix,yVec)
		implicit none
		
		integer											:: i, j, k, nx, ny, nxny, nrow
		real*8, dimension(:)								:: bVec, yVec
		real*8, dimension(:,:)							:: Lmatrix
		real											:: s
		
		nxny = size(bVec,1)
		
		do i = 1,nxny
			s = 0
			do j = 1,i-1
				s = s + yVec(j) * Lmatrix(i,j)
			enddo
			yVec(i) = (bVec(i) - s) / Lmatrix(i,i)
		enddo
	end subroutine Forward_Sub
	
	subroutine Backward_Sub(yVec,Umatrix,xVec)
		implicit none
		
		integer											:: i, j, k, nx, ny, nxny, nrow
		real*8, dimension(:)								:: xVec, yVec
		real*8, dimension(:,:)							:: Umatrix
		real											:: s
		
		nxny = size(yVec)
		
		do i = nxny,1,-1
			s = 0
			do j = i,nxny
				s = s + xVec(j) * Umatrix(i,j)
			enddo
			xVec(i) = (yVec(i) - s) / Umatrix(i,i)
		enddo
	end subroutine Backward_Sub
	
	subroutine SORSolveChoice(bVec, xVec, Nx, Ny, maxIter, BCFlag, dx, dy)
		implicit none
		
		integer, intent(in)								:: Nx, Ny, maxIter, BCFlag
		real*8, intent(in)								:: dx, dy
		real*8,dimension(:)								:: bVec, xVec
		
		select case (BCFlag)
			case (1)
				!print*, 'Solving using SOR on Dirichlet Boundary Conditions across all boundaries.'
				call Successive_OR_DBC(bVec,xVec, Nx, Ny, maxIter,dx, dy)
			case (2)
				!print*, 'Solving using SOR on Periodic Boundary Conditions across all boundaries.'
				call Successive_OR_1PBC(bVec, xVec, Nx, Ny, maxIter, dx, dy)
			case (3)
				!print*, 'This choice of SOR Solver is not yet implemented. Will be implemented in the next update.'
				!call Successive_OR_2PBC(bVec, xVec, Nx, Ny, maxIter, dx, dy)
			case (4)
				!print*, 'Solving using SOR on Neumann Boundary Conditions across all boundaries. Specifically no flux conditions as well.'
				call Successive_OR_Neu(bVec, xVec, Nx, Ny, maxIter, dx, dy)
			case (5)
				!print*, 'This choice of SOR Solver is not yet implemented. Will be implemented in the next update.'
				!call Successive_OR_Mixed
		end select
	
	end subroutine SORSolveChoice
	
	subroutine Successive_OR_Neu(bVec, xVec, Nx, Ny, maxIter, dx, dy)
		implicit none
		
		integer, intent(in)								:: Nx, Ny, maxIter
		real*8, intent(in)								:: dx, dy
		real											:: Res, gloRes, reFact, t_fact
		integer											:: i, j, nrow, nxny, iter
		real*8, dimension(:)								:: bVec, xVec
		real*8, dimension(:,:), allocatable				:: bMatrix, xMatrix, xMatrixOld
		
		allocate(bMatrix(0:Nx-1,0:Ny-1),xMatrix(0:Nx-1,0:Ny-1),xMatrixOld(0:Nx-1,0:Ny-1))
		
		t_fact = cos(3.14159/Nx) + cos(3.14159/Ny)
		reFact = (8 - sqrt(64 - 16*t_fact**2))/(t_fact**2)
		reFact =1.0
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				bMatrix(i,j) = bVec(nrow)
				xMatrix(i,j) = xVec(nrow)
			enddo
		enddo
		
		gloRes = 100.0
		iter = 0.0
		
		xMatrixOld = xMatrix
		
		do while (gloRes.gt.0.0001 .and. iter.le.maxIter)
			gloRes = 0.0
			iter = iter + 1
			do i = 0, Nx-1
				do j = 0, Ny-1
					if (i.eq.0) then
						if (j.eq.0) then
							Res = 0.25 * (2*xMatrixOld(i+1,j) + 2*xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						elseif (j.eq.Ny-1) then
							Res = 0.25 * (2*xMatrixOld(i+1,j) + 2*xMatrixOld(i,j-1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						else
							Res = 0.25 * (2*xMatrixOld(i+1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						endif
					elseif (i.eq.Nx-1) then
						if (j.eq.0) then
							Res = 0.25 * (2*xMatrixOld(i-1,j) + 2*xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						elseif (j.eq.Ny-1) then
							Res = 0.25 * (2*xMatrixOld(i-1,j) + 2*xMatrixOld(i,j-1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						else
							Res = 0.25 * (2*xMatrix(i-1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
						endif
					elseif (j.eq.0) then
						Res = 0.25 * (2*xMatrixOld(i,j+1) + xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
					elseif (j.eq.Ny-1) then
						Res = 0.25 * (2*xMatrixOld(i,j-1) + xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + bMatrix(i,j)*dx*dy) - xMatrixOld(i,j)
					else
						Res = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i-1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy)&
						- xMatrixOld(i,j)
					endif
					gloRes = max(abs(Res),gloRes)
					xMatrix(i,j) = xMatrixOld(i,j) + reFact*Res
				enddo
			enddo
			xMatrixOld = xMatrix
		enddo
		
		print*, 'This took ', iter, ' iterations to complete. Final max error was ', gloRes, '.'
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				xVec(nrow) = xMatrix(i,j)
			enddo
		enddo
		
	end subroutine Successive_OR_Neu
	
	subroutine SORSetup(xVec, Nx, Ny,Flag,bVec)
		implicit none
		
		integer, intent(in)								:: Nx, Ny,Flag
		real*8, dimension(:)								:: xVec, bVec
		integer											:: i,j,nrow,nxny
		
		select case (Flag)
			case (1)
				!All zeros
				do i = 0, Nx-1
					do j = 0, Ny-1
						nrow = (i)*Ny + j + 1
						xVec(nrow) = 0
					enddo
				enddo
			case (2)
				!All ones
				do i = 0, Nx-1
					do j = 0, Ny-1
						nrow = (i)*Ny + j + 1
						xVec(nrow) = 1
					enddo
				enddo
			case (3)
				!Random
				do i = 0, Nx-1
					do j = 0, Ny-1
						nrow = (i)*Ny + j + 1
						xVec(nrow) = rand()
					enddo
				enddo
			case (4)
				!Good Guess take from bVec
				do i = 0, Nx-1
					do j = 0, Ny-1
						nrow = (i)*Ny + j + 1
						xVec(nrow) = bVec(nrow)
					enddo
				enddo
			case (5)
				!Good guess from random close to 0.0
				do i = 0, Nx-1
					do j = 0, Ny-1
						nrow = (i)*Ny + j + 1
						xVec(nrow) = 0.1*rand() + 0.1 !varies between 0.1 and 0.2
					enddo
				enddo
		end select
	
	end subroutine SORSetup
	
	subroutine Successive_OR_1PBC(bVec, xVec, Nx, Ny, maxIter, dx, dy)
		implicit none
		
		integer, intent(in)								:: Nx, Ny, maxIter
		real*8, intent(in)								:: dx, dy
		real*8											:: Res, gloRes, reFact, t_fact
		integer											:: i, j, nrow, nxny, iter
		real*8, dimension(:)								:: bVec, xVec
		real*8, dimension(:,:), allocatable				:: bMatrix, xMatrix, xMatrixOld
		
		allocate(bMatrix(0:Nx-1,0:Ny-1),xMatrix(0:Nx-1,0:Ny-1),xMatrixOld(0:Nx-1,0:Ny-1))
		
		t_fact = cos(3.14159/Nx) + cos(3.14159/Ny)
		reFact = (8 - sqrt(64 - 16*t_fact**2))/(t_fact**2)
		reFact = 1.0
		nxny = Nx * Ny
		gloRes = 100.0
		iter = 0.0
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				bMatrix(i,j) = bVec(nrow)
				xMatrix(i,j) = xVec(nrow)
			enddo
		enddo
		
		xMatrixOld = xMatrix
		
		do while (gloRes.gt.0.0001 .and. iter.le.maxIter)
			gloRes = 0.0
			iter = iter + 1
			do i = 0, Nx-1
				do j = 0, Ny-1
					if (i.eq.0) then
						if (j.eq.0) then
							Res = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i,j+1) + xMatrixOld(Nx-1,j) + xMatrixOld(i,Ny-1) - bMatrix(i,j)*dx*dy) &
											- xMatrixOld(i,j)
						elseif (j.eq.Ny-1) then
							Res = 0.25 * (xMatrixOld(i+1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,0) + xMatrixOld(Nx-1,j) - bMatrix(i,j)*dx*dy) &
											- xMatrixOld(i,j)
						else
							Res = 0.25 * (xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + xMatrixOld(Nx-1,j) + xMatrixOld(i+1,j) - bMatrix(i,j)*dx*dy) &
											- xMatrixOld(i,j)
						endif
					elseif (i.eq.Nx-1) then
						if (j.eq.0) then
							Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i,j+1) + xMatrixOld(i,Ny-1) + xMatrixOld(0,j) - bMatrix(i,j)*dx*dy) &
											- xMatrixOld(i,j)
						elseif (j.eq.Ny-1) then
							Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,0) + xMatrixOld(0,j) - bMatrix(i,j)*dx*dy) &
											- xMatrixOld(i,j)
						else
							Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + xMatrixOld(0,j) - bMatrix(i,j)*dy*dx) &
											- xMatrixOld(i,j)
						endif
					elseif (j.eq.0) then
						Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i+1,j) + xMatrixOld(i,j+1) + xMatrixOld(i,Ny-1) - bMatrix(i,j)*dy*dx) &
										- xMatrixOld(i,j)
					elseif (j.eq.Ny-1) then
						Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i+1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,0) - bMatrix(i,j)*dx*dy) &
										- xMatrixOld(i,j)
					else
						Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i+1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) - bMatrix(i,j)*dx*dy) &
										- xMatrixOld(i,j)
					endif
					gloRes = max(abs(Res),gloRes)
					xMatrix(i,j) = xMatrixOld(i,j) + Res
				enddo
			enddo
			print*, 'gloRes is ', gloRes
			xMatrixOld = xMatrix
		enddo
		
		print*, 'This took ', iter, ' iterations to complete. Final max error was ', gloRes, '.'
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				xVec(nrow) = xMatrix(i,j)
			enddo
		enddo
	
	end subroutine Successive_OR_1PBC
	
	subroutine Successive_OR_DBC(bVec, xVec, Nx, Ny, maxIter, dx, dy)
		implicit none
		
		integer, intent(in)								:: Nx, Ny, maxIter
		real*8, intent(in)								:: dx, dy
		real*8											:: dx2, dy2, Res, gloRes, reFact, t_fact
		integer											:: i, j, nrow, nxny, iter
		real*8, dimension(:)								:: bVec, xVec
		real*8, dimension(:,:), allocatable				:: bMatrix, xMatrix, xMatrixOld
		
		allocate(bMatrix(0:Nx-1,0:Ny-1),xMatrix(0:Nx-1,0:Ny-1),xMatrixOld(0:Nx-1,0:Ny-1))
		
		t_fact = cos(3.14159/Nx) + cos(3.14159/Ny)
		reFact = (8 - sqrt(64 - 16*t_fact**2))/(t_fact**2)
		reFact = 1.0
		print*, 'reFact is ',reFact
		
		nxny = Nx * Ny
		gloRes = 100.0
		iter = 0
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				bMatrix(i,j) = bVec(nrow)
				xMatrix(i,j) = xVec(nrow)
			enddo
		enddo
		
		xMatrixOld = xMatrix
		open(unit=13,file='Conver.dat',action='write',status='replace')

		do while (gloRes.gt.0.0001 .and. iter.le.maxIter)
			iter = iter + 1
			gloRes = 0.0
			do i = 1, Nx-2
				do j = 1, Ny-2
					Res = 0.25 * (xMatrixOld(i-1,j) + xMatrixOld(i+1,j) + xMatrixOld(i,j-1) + xMatrixOld(i,j+1) + bMatrix(i,j)*dx*dy) &
									- xMatrixOld(i,j)
					gloRes = max(abs(Res),gloRes)
					xMatrix(i,j) = xMatrixOld(i,j) + reFact*Res
				enddo
			enddo
			write(13,*) iter,',',gloRes
			xMatrixOld = xMatrix
		enddo
		
		close(13)
		
		print*, 'This took ', iter, ' iterations to complete. Final max error was ', gloRes, '.'
		
		do i = 0, Nx-1
			do j = 0, Ny-1
				nrow = (i)*Ny + j + 1
				xVec(nrow) = xMatrix(i,j)
			enddo
		enddo
		
	end subroutine Successive_OR_DBC
	
end module DirectSolving