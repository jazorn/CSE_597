%Plotting Script for CSE597 Poisson Script
%Written by Jacob Zorn
%Written: 9/19/2018
%Version: 1.0.0.

nx = input('How many elements in the "x" direction?');
ny = input('How many elements in the "y" direction?');

matr = load('xVec.dat');
plot_matr = zeros(nx,ny);

for i = 1:nx
	for j = 1:ny
		nrow = (i-1)*ny + j;
		plot_matr(i,j) = matr(nrow);
	end
end

fprintf('Now plotting the Electric Potential.')
h = surf(plot_matr); colormap jet; colorbar;
saveas(h,'fig_xVec.png');

%Derive the electric field
%Recall that the electric field is given by:
%			E = d(o(x,y)); where o is phi
%

[elec_x, elec_y] = gradient(plot_matr);
[X,Y] = meshgrid(0:1:nx,0:1:ny);

fprintf('Now plotting the Electric Field.');
h = quiver(X,Y,elec_x,elec_y);
saveas(h,'fig_ElecField.png');

%Plot a contour map with the electric field superimposed

fprintf('Now plotting contour plot with superimposed electric field.')
h = contourf(plot_matr); colormap jet; colorbar;
hold on;
quiver(X,Y,elec_x,elec_y);
saveas(h,'fig_Contour.png');

fprintf('Done. Now Closing Matlab')