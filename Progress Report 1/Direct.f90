!Direct Solver for Poisson's Equation
!Written for CSE597
!Pennsylvania State University
!Written by Jacob Zorn
!9/11/2018
!Must Compile and use the DirectSolving Module: DirectSolving.f90
!Need to check my Neumann Direct Solver

include "DirectSolving.f90"

program main
	
	use DirectSolving
	implicit none
	
	integer												:: i, j, k, nx,ny,nxny,nrow,an, CBC, CSOL, maxIter, InitalFlag
	real*8, dimension(:,:), allocatable					:: Lmatrix, Umatrix, Amatrix
	character*80										:: myFile
	real*8, dimension(:), allocatable					:: bVec, yVec, xVec
	real*8												:: start, finish, dx, dy
	
	call Read_Input(nx, ny, CBC, CSOL, dx, dy, InitalFlag)
	nxny = nx*ny

	allocate(bVec(0:nxny-1),yVec(0:nxny-1),xVec(0:nxny-1))
		
	do i = 0,nxny-1
		if (i .eq. (nx/2 * ny + nx/2)) then
			bVec(i) = -1
		else
			bVec(i) = 0
		endif
		yVec(i) = 0
		xVec(i) = 0
	enddo
		
	!call SetBoundaryConditions(nx, ny, CBC, Amatrix)
	if (CSOL.eq.1) then
		call cpu_time(start)
		allocate(Lmatrix(0:nxny-1,0:nxny-1),Umatrix(0:nxny-1,0:nxny-1),Amatrix(0:nxny-1,0:nxny-1))
		call SetBoundaryConditions(nx,ny,CBC,Amatrix)
		call LU_Decomp_NoPivot(Amatrix,Lmatrix,Umatrix)
		call Forward_Sub(bVec,Lmatrix,yVec)
		call Backward_Sub(yVec,Umatrix,xVec)
		call cpu_time(finish)
		print*, 'Time to complete LU_Decomp for ', nxny*nxny, ' elements was ', 1000000*(finish - start), ' seconds.'
		myFile = 'xVec.dat'
		call Write_Vector(xVec,myFile)
		myFile = 'bVec.dat'
		call Write_Vector(bVec, myFile)
		deallocate(Lmatrix,Umatrix,Amatrix,bVec,yVec,xVec)
	else
		call SORSetup(xVec, Nx, Ny, InitalFlag, bVec)
		call cpu_time(start)
		maxiter = 20000
		call SORSolveChoice(bVec,xVec,nx,ny,maxIter,CBC,dx,dy)
		call cpu_time(finish)
		print*, 'Time to complete an iterative solution for ', nx * ny, ' elements was ', 1000000*(finish - start), ' seconds.'
		myFile = 'xVec.dat'
		call Write_Vector(xVec,myFile)
		myFile = 'bVec.dat'
		call Write_Vector(bVec, myFile)	
		deallocate(bVec,xVec)
	endif
	
end program main
	